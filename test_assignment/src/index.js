import "./style.scss";

function removeColorName(colorCheckbox, color) {

    if(colorCheckbox.checked == false) {
        for(let z=0; z < colorNames.children.length; z++) {
            if(colorNames.children[z].innerText == color) {
                colorNames.children[z].remove();
            }
        }
    }
}

//Price Dropdown
const priceToggle = document.querySelector(".price-dropdown-toggle");
const priceDropdown = document.querySelector(".price-range-container");
const priceArrow = document.querySelector(".price-toggle-arrow");
priceDropdown.style.display = 'none';

priceToggle.addEventListener("click", () => {


    if(priceDropdown.style.display == "none") {
        priceDropdown.style.display = "block";
        priceArrow.style.transform = "rotate(180deg)";
    } else {
        priceDropdown.style.display = "none";
        priceArrow.style.transform = "rotate(0deg)";
    }

    if(brandDropdown.style.display == "block") {
        brandDropdown.style.display = "none";
        brandArrow.style.transform = "rotate(0deg)";        
    }

    if(colorDropdown.style.display == "block") {
        colorDropdown.style.display = "none";
        colorArrow.style.transform = "rotate(0deg)";        
    }
});

let minSlider = document.getElementById('minimum');
let maxSlider = document.getElementById('maximum');

let outputMin = document.getElementById('minValue');
let outputMax = document.getElementById('maxValue');

outputMin.innerHTML = minSlider.value;
outputMax.innerHTML = maxSlider.value;

minSlider.oninput = function() {
    outputMin.innerHTML = this.value;
}

maxSlider.oninput = function() {
    outputMax.innerHTML = this.value;
}

//Brand Dropdown
const brandToggle = document.querySelector(".brand-dropdown-toggle");
const brandDropdown = document.querySelector(".brand-menu");
const brandArrow = document.querySelector(".brand-toggle-arrow");
brandDropdown.style.display = 'none';

brandToggle.addEventListener("click", () => {

    
    if(brandDropdown.style.display == "none") {
        brandArrow.style.transform = "rotate(180deg)";
        brandDropdown.style.display = "block";
    } else {
        brandDropdown.style.display = "none";
        brandArrow.style.transform = "rotate(0deg)";
    }
    
    if(priceDropdown.style.display == "block") {
        priceDropdown.style.display = "none";   
        priceArrow.style.transform = "rotate(0deg)";         
    }
    
    if(colorDropdown.style.display == "block") {
        colorDropdown.style.display = "none";        
        colorArrow.style.transform = "rotate(0deg)";         
    }


});


//Color Dropdown
const colorToggle = document.querySelector(".color-dropdown-toggle");
const colorDropdown = document.querySelector(".product-color-container");
const colorArrow = document.querySelector(".color-toggle-arrow");
colorDropdown.style.display = 'none';

colorToggle.addEventListener("click", () => {


    if(colorDropdown.style.display == "none") {
        colorDropdown.style.display = "block";
        colorArrow.style.transform = "rotate(180deg)";
    } else {
        colorDropdown.style.display = "none";
        colorArrow.style.transform = "rotate(0deg)";
    }

    if(brandDropdown.style.display == "block") {
        brandDropdown.style.display = "none";
        brandArrow.style.transform = "rotate(0deg)";        
    }
    
    if(priceDropdown.style.display == "block") {
        priceDropdown.style.display = "none";        
        priceArrow.style.transform = "rotate(0deg)";        
    }
});

const colorContainer = document.querySelector(".colors");
const colorNames = document.querySelector(".color-names");
const name = document.querySelector(".name");
const svg = '<svg class="x" width="16" height="16" viewBox="0 0 16 16" fill="none" xmlns="http://www.w3.org/2000/svg"> <path d="M12 4L4 12" stroke="black" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/> <path d="M4 4L12 12" stroke="black" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/> </svg>';
const colorDiv = document.querySelector(".color");

const blackCheckbox = colorContainer.children[0];
const black = document.querySelector(".black");
const blackSvg = black.children[0];

blackCheckbox.addEventListener('click', function() {

    if(blackCheckbox.checked) {
        blackSvg.style.display = "block";
    } else {
        blackSvg.style.display = "none";
    }
});

const redCheckbox = colorContainer.children[2];
const red = document.querySelector(".red");
const redSvg = red.children[0];

redCheckbox.addEventListener('click', function() {

    if(redCheckbox.checked) {
        redSvg.style.display = "block";
    } else {
        redSvg.style.display = "none";
    }
});

const indigoCheckbox = colorContainer.children[4];
const inding = document.querySelector(".indigo");
const indigoSvg = inding.children[0];

indigoCheckbox.addEventListener('click', function() {

    if(indigoCheckbox.checked) {
        indigoSvg.style.display = "block";
    } else {
        indigoSvg.style.display = "none";
    }
});

const greenCheckbox = colorContainer.children[6];
const green = document.querySelector(".green");
const greenSvg = green.children[0];

greenCheckbox.addEventListener('click', function() {

    if(greenCheckbox.checked) {
        greenSvg.style.display = "block";
    } else {
        greenSvg.style.display = "none";
    }
});

const yellowCheckbox = colorContainer.children[8];
const yellow = document.querySelector(".yellow");
const yellowSvg = yellow.children[0];

yellowCheckbox.addEventListener('click', function() {

    if(yellowCheckbox.checked) {
        yellowSvg.style.display = "block";
    } else {
        yellowSvg.style.display = "none";
    }
});

const orangeCheckbox = colorContainer.children[10];
const orange = document.querySelector(".orange");
const orangeSvg = orange.children[0];

orangeCheckbox.addEventListener('click', function() {

    if(orangeCheckbox.checked) {
        orangeSvg.style.display = "block";
    } else {
        orangeSvg.style.display = "none";
    }
});

const greyCheckbox = colorContainer.children[12];
const grey = document.querySelector(".grey");
const greySvg = grey.children[0];

greyCheckbox.addEventListener('click', function() {

    if(greyCheckbox.checked) {
        greySvg.style.display = "block";
    } else {
        greySvg.style.display = "none";
    }
});

const whiteCheckbox = colorContainer.children[14];
const white = document.querySelector(".white");
const whiteSvg = white.children[0];

whiteCheckbox.addEventListener('click', function() {
    
    if(whiteCheckbox.checked) {
        whiteSvg.style.display = "block";
    } else {
        whiteSvg.style.display = "none";
    }
});

//Slider

let swiper = new Swiper(".slide-content", {
    slidesPerView: 5,
    spaceBetween: 30,
    slidesPerGroup: 1,
    loop: true,
    loopFillGroupWithBlank: true,
    pagination: {
      el: ".swiper-pagination",
      clickable: true,
    },
    navigation: {
      nextEl: ".swiper-btn-next",
      prevEl: ".swiper-btn-prev",
    },
});

// Add To Cart Number
let addToCartBtn = document.querySelectorAll(".add-to-cart");
let cartProductNumber = document.querySelector(".cart-product-number");
let count = 0;

if(cartProductNumber.innerHTML.length == 0) {
    cartProductNumber.innerHTML = 0;
}
for(let i=0; i < addToCartBtn.length; i++) {
    addToCartBtn[i].addEventListener("click", function () {
        count++;
        cartProductNumber.innerHTML = count;
    });
}

//Product Color
const colorsContainer = document.querySelector(".colors");
const colorInput = colorsContainer.querySelectorAll(".inputCheck");

for(let y=0; y < colorInput.length; y++) {
    colorInput[y].addEventListener("change", function () {
            
        let listItem = '<li class="color-title" value="' + colorInput[y].value + '">' + svg + '<span class="name">'+ colorInput[y].value +'</span> </li>';
        colorNames.innerHTML += listItem;
        console.log(colorInput[y].value);
    });
}